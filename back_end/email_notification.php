<?
$_SERVER['DOCUMANT_ROOT'] = '....'; // т.к. скрипт испольняется из консоли явно зададим документ руут
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if(!function_exists('actual_prod')) {
    /**
     * @array $ar1
     * @array $ar2
     * @array mixed
     *
     * Функция для проверки купленных и отложенных товаров в массивах
     */
    function actual_prod($ar1, $ar2)
    {
        foreach ($ar1 as $k => $v) {
            if (in_array($v, $ar2)) {
                unset($ar1[$k]);
            }
        }
        return $ar1;
    }
}
Cmodule::IncludeModule('sale');
Cmodule::IncludeModule('iblock');

$date_mount  = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), (time()-(30*24*60*60)));

// выберем все отложенные товары за последний месяц
$dbBasketItems = CSaleBasket::GetList(array("NAME" => "ASC","ID" => "ASC"),
    array("LID" => SITE_ID,"DELAY" => 'Y','>DATE_INSERT'=>$date_mount),
    false,false, array("ID", "PRODUCT_ID",'USER_ID')
);
while ($arItems = $dbBasketItems->Fetch())
{
    // соберем отложенные продукты по каждому пользователю
    $users_delay_products[$arItems['USER_ID']][] = $arItems['PRODUCT_ID'];
    $user_ids[] = $arItems['USER_ID'];
    $products_ids[] = $arItems['PRODUCT_ID'];
}

// выберем имена всех отложенных товаров для отправки в письмах
$res = CiblockElement::GetList(array(),array('IBLOCK_ID'=>1,'ID'=>$products_ids),false,false,array('ID','NAME'));
while($row = $res->GetNext()){
    $products_names[$row['ID']] = $row['NAME'];
}

// выберем все купленные товары за последний месяц, теми пользователями которые откладывали товар
$dbBasketItems = CSaleBasket::GetList(array("NAME" => "ASC","ID" => "ASC"),
    array("LID" => SITE_ID,"DELAY" => 'N','>DATE_INSERT'=>$date_mount,'USER_ID'=>$user_ids),
    false,false, array("ID", "PRODUCT_ID",'USER_ID')
);
while ($arItems = $dbBasketItems->Fetch())
{
    $users_by_products[$arItems['USER_ID']][] = $arItems['PRODUCT_ID'];
}

// выбираем пользователей для рассылки
$user_ids_filter = implode('|',$user_ids);
$res_user = Cuser::GetList(($by="personal_country"), ($order="desc"),
                            array('ID'=>$user_ids_filter),
                            array('FIELDS'=>(array('ID','NAME','LAST_NAME','EMAIL'))));
while($row_user = $res_user->fetch()){
    // соберем финальный массив продуктов для пользователя, что бы не было пересечений отложен и уже был куплен
    $actual_product = actual_prod($users_delay_products[$row_user['ID']],$users_by_products[$row_user['ID']]);
    $actual_product = array_unique($actual_product);
    foreach($actual_product as $p){
        $actual_product_name[] = $products_names[$p];
    }
    $row_user['PRODUCTS'] = $actual_product_name;
    $users_array[$row_user['ID']] = $row_user;
}

/**
 * отправка писем. предположим что есть почтовый шаблон, отправляем по нему. так же сам список товаров,
 * сейчас будет через запятую имена. но можно доработать под нужный красивый вид
 */
foreach($users_array as $user){
    $msg_array = array('NAME'=>$user['NAME'].' '.$user['LAST_NAME'],
                       'EMAIL'=>$user['EMAIL'],'PRODUCT_LIST'=>implode(',',$user['PRODUCTS']));
    Cevent::Send('DELAY_USER_NOTIFICATION',array('s1'),$msg_array);
}